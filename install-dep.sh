#!/bin/bash

# top menu
x=0
while [ $x = 0 ]
do
	clear
	echo "---MENU---"
	echo "1 - Install Dependencys"
	echo "2 - Exit Installer"
	echo
	read -p "Enter number of option: " option1

	case "$option1" in
		2)
		clear
		echo "Goodbye"
		x=1
		;;

		1)
		# Disto menu
		x=0
		while [ $x = 0 ]
		do
			clear
			echo "Disto Menu"
			echo "debian"
			echo "arch"
			echo "ubuntu"
			read -p "What distro do you have: " option2

			case "$option2" in
				debian)
				sudo apt update
				sudo apt-get install \
					apt-transport-https \
					ca-certificates \
					curl \
					gnupg-agent \
					software-properties-common -y
				curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -
				sudo apt-key fingerprint 0EBFCD88
				sudo add-apt-repository \
					"deb [arch=amd64] https://download.docker.com/linux/debian \
					$(lsb_release -cs) \
					stable"
				sudo apt update
				sudo apt-get install docker-ce docker-ce-cli containerd.io
				sudo usermod -aG docker $USER
				x=1
				;;
				arch)
				sudo pacman -Syu
				sudo pacman -S docker --noconfirm -y
				sudo systemctl start docker
				sudo systemctl enable docker
				sudo usermod -aG docker $USER
				x=1
				;;
				ubuntu)
				sudo apt-get remove docker docker-engine docker.io containerd runc
				sudo apt update
				sudo apt-get install \
    				apt-transport-https \
    				ca-certificates \
   					curl \
    				gnupg-agent \
    				software-properties-common
				curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
				sudo apt-key fingerprint 0EBFCD88
				sudo add-apt-repository \
   					"deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   					$(lsb_release -cs) \
   					stable"
				sudo apt update
				sudo apt-get install docker-ce docker-ce-cli containerd.io
				sudo usermod -aG docker $USER
				x=1
				;;
				*)
				clear
				echo "your distro is not supported yet"
				x=1
				;;
			esac
		done
		x=1
		;;

		*)
		echo "try an other option"
		clear
		sleep 3
		;;
	esac
done
