# Dock app

## What is it
This is a project aims to create a docker based package manager for any linux distro. It works off a series of shell scripts that create docker containers with gui applications running inside them and presented through X11, The curated scripts would also create a .desktop file to easily run the apps.

## Powered by the Community
Because i am only one person each application that is not supported yet i will provide a template script for you the community to create your own containerized apps.

## Warning
this project is very early in it's development
